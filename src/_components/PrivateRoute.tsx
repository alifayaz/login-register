import React, {FC, ReactElement} from 'react';
import { Route, Redirect } from 'react-router-dom';

type Props = {
    roles?: any,
    component?: any;
    exact?: any;
    path?: any
  }

const PrivateRoute: FC<Props> = ({
    component: Component, roles, ...rest}): ReactElement => {
    return (
        <Route {...rest} render={props => {
            if (!localStorage.getItem('user')) {
                // not logged in so redirect to login page with the return url
                return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            }

            // logged in so return component
            return <Component {...props} />
        }} />
    );
}

export { PrivateRoute };