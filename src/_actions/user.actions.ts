import { userConstants } from '../_constants';
import { userService } from '../_services';
import { alertActions } from './alert.actions';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    register,
    getAll,
    delete: _delete
};

function login(username: string, password: string, from: any) {
    return (dispatch: any) => {
        dispatch(request( {username} ));

        userService.login(username, password)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push("/");
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user: object) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user: object) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error: string) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function register(user: object) {
    return (dispatch: any) => {
        dispatch(request(user));

        userService.register(user)
            .then(
                user => { 
                    dispatch(success({}));
                    history.push('/login');
                    dispatch(alertActions.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user: object) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user: object) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error: string) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function getAll() {
    return (dispatch: any) => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users: any) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error: string) { return { type: userConstants.GETALL_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id: number) {
    return (dispatch: any) => {
        dispatch(request(id));

        userService.delete(id)
            .then(
                user => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id: number) { return { type: userConstants.DELETE_REQUEST, id } }
    function success(id: number) { return { type: userConstants.DELETE_SUCCESS, id } }
    function failure(id: number, error: string) { return { type: userConstants.DELETE_FAILURE, id, error } }
}